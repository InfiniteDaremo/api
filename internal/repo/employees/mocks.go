package employees

import (
	"context"
	"gitlab.com/InfiniteDaremo/api/internal/datastore"
	"gitlab.com/InfiniteDaremo/api/internal/errors"
	"gitlab.com/InfiniteDaremo/api/models"
	"time"
)

type MockEmployeeStore struct {
	employees []*models.Employee
}

func NewMockEmployeeStore() *MockEmployeeStore {
	return &MockEmployeeStore{
		employees: []*models.Employee{
			{
				ID:   0,
				Name: "gavin",
				Dob:  time.Now(),
			},
			{
				ID:   1,
				Name: "alex",
				Dob:  time.Now(),
			},
		},
	}
}

func (es *MockEmployeeStore) GetEmployees(_ context.Context, _ datastore.IDatastore) ([]*models.Employee, error) {
	return es.employees, nil
}

func (es *MockEmployeeStore) GetEmployee(_ context.Context, _ datastore.IDatastore, name string) (*models.Employee, error) {
	for _, e := range es.employees {
		if e.Name == name {
			return e, nil
		}
	}

	return nil, &errors.EmployeeNotFoundError{Name: name}
}

var _ EmployeeStoreInterface = (*MockEmployeeStore)(nil)
