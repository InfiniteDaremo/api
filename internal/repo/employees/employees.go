package employees

import (
	"context"
	"database/sql"
	"gitlab.com/InfiniteDaremo/api/internal/datastore"
	"gitlab.com/InfiniteDaremo/api/internal/errors"
	"gitlab.com/InfiniteDaremo/api/models"
)

type EmployeeStoreInterface interface {
	GetEmployees(context.Context, datastore.IDatastore) ([]*models.Employee, error)
	GetEmployee(context.Context, datastore.IDatastore, string) (*models.Employee, error)
}

type EmployeeStore struct {
}

func (es *EmployeeStore) GetEmployees(ctx context.Context, db datastore.IDatastore) ([]*models.Employee, error) {
	e, err := models.Employees().All(ctx, db)
	if err != nil {
		return nil, err
	}
	return e, nil
}

func (es *EmployeeStore) GetEmployee(ctx context.Context, db datastore.IDatastore, name string) (*models.Employee, error) {
	e, err := models.Employees(models.EmployeeWhere.Name.EQ(name)).One(ctx, db)
	switch {
	case err == sql.ErrNoRows:
		return nil, &errors.EmployeeNotFoundError{Name: name}
	case err != nil:
		return nil, err
	}
	return e, nil
}

var _ EmployeeStoreInterface = (*EmployeeStore)(nil)
