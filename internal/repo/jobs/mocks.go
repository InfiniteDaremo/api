package jobs

import (
	"context"
	"github.com/volatiletech/null/v8"
	"gitlab.com/InfiniteDaremo/api/internal/datastore"
	"gitlab.com/InfiniteDaremo/api/internal/errors"
	"gitlab.com/InfiniteDaremo/api/models"
	"time"
)

type MockJobsStore struct {
	jobs []*models.Job
}

func NewMockJobStore() *MockJobsStore {
	job := &models.Job{
		ID:          0,
		EmployeeID:  0,
		Title:       "Test",
		Role:        "Test Role",
		Description: "Description",
		StartDate:   time.Time{},
		EndDate:     null.Time{},
	}
	job.R = job.R.NewStruct()
	job.R.Achievements = []*models.Achievement{
		{
			ID:          0,
			JobID:       0,
			Description: "Test",
		},
	}

	return &MockJobsStore{
		jobs: []*models.Job{
			job,
		},
	}
}

func (m MockJobsStore) GetJobs(_ context.Context, _ datastore.IDatastore, employee *models.Employee) ([]*models.Job, error) {
	var jobs []*models.Job
	for _, e := range m.jobs {
		if e.EmployeeID == employee.ID {
			jobs = append(jobs, e)
		}
	}

	if len(jobs) == 0 {
		return nil, &errors.NoContent{}
	}

	return jobs, nil
}

var _ JobStoreInterface = (*MockJobsStore)(nil)
