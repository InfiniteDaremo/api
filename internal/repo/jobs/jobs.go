package jobs

import (
	"context"
	"database/sql"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/InfiniteDaremo/api/internal/datastore"
	"gitlab.com/InfiniteDaremo/api/internal/errors"
	"gitlab.com/InfiniteDaremo/api/models"
)

type JobStoreInterface interface {
	GetJobs(context.Context, datastore.IDatastore, *models.Employee) ([]*models.Job, error)
}

type JobStore struct {
}

func (j JobStore) GetJobs(ctx context.Context, store datastore.IDatastore, employee *models.Employee) ([]*models.Job, error) {
	qs := []qm.QueryMod{
		models.JobWhere.EmployeeID.EQ(employee.ID),

		// Eager load achievements
		qm.Load(models.JobRels.Achievements),
	}

	jobs, err := models.Jobs(qs...).All(ctx, store)
	switch {
	case err == sql.ErrNoRows, jobs == nil:
		return nil, &errors.NoContent{}
	case err != nil:
		return nil, err
	}

	return jobs, err
}

var _ JobStoreInterface = (*JobStore)(nil)
