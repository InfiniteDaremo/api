package repo

import (
	"gitlab.com/InfiniteDaremo/api/internal/repo/employees"
	"gitlab.com/InfiniteDaremo/api/internal/repo/jobs"
)

type Repo struct {
	Employees employees.EmployeeStoreInterface
	Jobs jobs.JobStoreInterface
}

func NewRepo() *Repo {
	return &Repo{
		Employees: &employees.EmployeeStore{},
		Jobs: &jobs.JobStore{},
	}
}

func NewMockRepo() *Repo {
	return &Repo{
		Employees: employees.NewMockEmployeeStore(),
		Jobs: jobs.NewMockJobStore(),
	}
}
