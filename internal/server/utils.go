package server

type serverError interface {
	error
	Code() int
}

type notFoundError interface {
	serverError
	NotFound()
}

type noContentError interface {
	serverError
	NoContent()
}