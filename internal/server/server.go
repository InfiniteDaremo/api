package server

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/InfiniteDaremo/api/internal/datastore"
	"gitlab.com/InfiniteDaremo/api/internal/middleware"
	"gitlab.com/InfiniteDaremo/api/internal/repo"
	"gitlab.com/InfiniteDaremo/api/internal/spec"
	"go.uber.org/zap"
)

//go:generate go run github.com/deepmap/oapi-codegen/cmd/oapi-codegen --config=../spec/types.cfg.yaml ../../openapi/swagger.yaml
//go:generate go run github.com/deepmap/oapi-codegen/cmd/oapi-codegen --config=../spec/server.cfg.yaml ../../openapi/swagger.yaml

type Server struct {
	ds     datastore.IDatastore
	repo   *repo.Repo
	logger *zap.Logger
}

func NewServer(ds datastore.IDatastore, repo *repo.Repo, logger *zap.Logger) *Server {
	return &Server{
		ds:     ds,
		repo:   repo,
		logger: logger,
	}
}

func RegisterHandlers(e *echo.Echo, s *Server, repo *repo.Repo, store datastore.IDatastore) {
	wrapper := spec.ServerInterfaceWrapper{
		Handler: s,
	}

	eg := e.Group("/employees")
	eg.Use(middleware.EmployeeMiddleware(repo, store))
	eg.GET("/:name", wrapper.GetEmployeesName)
	eg.GET("/:name/jobs", wrapper.GetEmployeesNameJobs)

	// Setup after to allow /employees root
	e.GET("/employees", wrapper.GetEmployees)
}

// Interface assertions
var _ spec.ServerInterface = (*Server)(nil)
