package server

import (
	"errors"
	"github.com/labstack/echo/v4"
	"gitlab.com/InfiniteDaremo/api/internal/spec"
	"gitlab.com/InfiniteDaremo/api/internal/view"
	"net/http"
)

func (s Server) GetEmployees(ctx echo.Context) error {
	e, err := s.repo.Employees.GetEmployees(ctx.Request().Context(), s.ds)
	if err != nil {
		return ctx.String(http.StatusInternalServerError, err.Error())
	}

	employees := make([]*spec.Employee, len(e))
	for i, ee := range e {
		employees[i] = view.EmployeeView(ee)
	}

	return ctx.JSON(http.StatusOK, employees)
}

func (s Server) GetEmployeesName(ctx echo.Context, name spec.Name) error {
	e, err := s.repo.Employees.GetEmployee(ctx.Request().Context(), s.ds, string(name))
	if err != nil {
		var notFound notFoundError
		switch {
		case errors.As(err, &notFound):
			return ctx.String(notFound.Code(), notFound.Error())
		default:
			return ctx.String(http.StatusInternalServerError, err.Error())
		}
	}

	return ctx.JSON(http.StatusOK, view.EmployeeView(e))
}