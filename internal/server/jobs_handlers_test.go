package server

import (
	"net/http"
	"testing"

	"github.com/deepmap/oapi-codegen/pkg/testutil"
	"github.com/stretchr/testify/assert"
)

func Test_GetEmployeeJobs(t *testing.T) {
	res := testutil.NewRequest().Get("/employees/gavin/jobs").Go(t, e)
	assert.Equal(t, http.StatusOK, res.Code())

	res = testutil.NewRequest().Get("/employees/missing/jobs").Go(t, e)
	assert.Equal(t, http.StatusNotFound, res.Code())

	res = testutil.NewRequest().Get("/employees/alex/jobs").Go(t, e)
	assert.Equal(t, http.StatusNoContent, res.Code())
}
