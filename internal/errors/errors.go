package errors

import (
	"fmt"
	"net/http"
)

type NotFoundError struct {
}

func (e *NotFoundError) Error() string { return "not found" }
func (e *NotFoundError) NotFound()     {}
func (e *NotFoundError) Code() int     { return http.StatusNotFound }

type EmployeeNotFoundError struct {
	Name string
}

func (e *EmployeeNotFoundError) Error() string { return fmt.Sprintf("employee not found: %s", e.Name) }
func (e *EmployeeNotFoundError) NotFound()     {}
func (e *EmployeeNotFoundError) Code() int     { return http.StatusNotFound }

type NoContent struct {
}

func (e *NoContent) Error() string { return "no content" }
func (e *NoContent) NoContent()     {}
func (e *NoContent) Code() int     { return http.StatusNoContent }