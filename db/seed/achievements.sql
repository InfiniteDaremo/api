DO $$
    DECLARE jid int;
BEGIN
--  Laughing Jackal Achievements
    SELECT id from jobs WHERE title = 'Laughing Jackal' INTO jid;

    INSERT INTO achievements (job_id, description) VALUES
        (jid, 'Created Playstation Vita port of Hungry Giraffe'),
        (jid, 'Created iOS port of Hungry Giraffe'),
        (jid, 'Created Playstation Mobile port of Hungry Giraffe'),
        (jid, 'Created Orbit on PSP'),
        (jid, 'Created OMG-Z available on PSP');

--  Atom Bank Achievements
    SELECT id from jobs WHERE title = 'Atom Bank' AND role = 'Lead Middleware Developer' INTO jid;

    INSERT INTO achievements (job_id, description) VALUES
        (jid, 'Developed the entire Middleware replacement, all of the teams tooling for CICD and error reporting as well as the Faster Payment pipeline');

--  HSBC Achievements
    SELECT id from jobs WHERE title = 'HSBC' INTO jid;

    INSERT INTO achievements (job_id, description) VALUES
        (jid, 'Migrated legacy systems onto the Mulesoft Middleware stack'),
        (jid, 'Created review automation tools to check all Mule files met minimum standards and security protocols, the manual review process took upto an hour and the tool was able to reduce this to a matter of seconds');

--  Blockdaemon Achievements
    SELECT id from jobs WHERE title = 'Blockdaemon' INTO jid;

    INSERT INTO achievements (job_id, description) VALUES
        (jid, 'Created the new website using React JS'),
        (jid, 'Wrote the Vault GCP infrastructure in Terraform'),
        (jid, 'Integrated Polkadot, Algorand and Ripple protocols into Ubiquity'),
        (jid, 'Created a Staking Report service which generates CSV file dumps of an accounts activity for a specific protocol');
END $$;



