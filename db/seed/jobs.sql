CREATE OR REPLACE FUNCTION date_ts(date text)
    RETURNS timestamp
    LANGUAGE plpgsql
AS $$
BEGIN
    RETURN to_timestamp(date, 'YYYY-MM');
END $$;

DO $$
    DECLARE gavin_id int;
BEGIN
    SELECT id from employees WHERE name = 'gavin' INTO gavin_id;

    INSERT INTO jobs (employee_id, title, role, description, start_date, end_date) VALUES
        (gavin_id, 'Big Bear Studios', 'Mobile Games Developer', 'Various projects developed for the iPhone platform. Involved programming in C++ / Object C across 6 titles', date_ts('2009-03-01'), date_ts('2009-12-01')),
        (gavin_id, 'Fluid Pixel', 'Mobile Games and Application Developer', 'Various iPhone projects. Involved programming in C++ / Object C across 6 titles. 2 titles were developed with the Unity engine', date_ts('2010-01-01'), date_ts('2011-03-01')),
        (gavin_id, 'Laughing Jackal', 'Games Developer', 'At Laughing Jackal I created various games for Sony platforms and PC. I developed internal tooling and working on the companies proprietary engine', date_ts('2011-03-01'), date_ts('2015-09-01')),
        (gavin_id, 'Atom Bank', 'Middleware Developer', 'At Atom I have developed the mobile banking app within the Unity Engine along with the native plugins for iOS. I later moved into Middleware and Security development with the Mulesoft ESB platform', date_ts('2015-08-01'), date_ts('2017-03-01')),
        (gavin_id, 'Atom Bank', 'Lead Middleware Developer', 'My role as Lead involved PoC''s, Team, Sprint and Stakeholder Management as well as general programming in Mulesoft, Python and Java', date_ts('2017-03-01'), date_ts('2018-04-01')),
        (gavin_id, 'Dorril Woods Ltd', 'Company Founder', 'Started my own software development company alongside my wife', date_ts('2018-04'), NULL),
        (gavin_id, 'HSBC', 'Lead Mulesoft Developer', 'Integrated legacy platforms into Service now via Mulesoft middleware. Created various tools to improve productivity and coding standards', date_ts('2018-04'), date_ts('2019-04')),
        (gavin_id, 'Blockdaemon', 'Full Stack Developer', 'Created and maintained a new website, created various API services in GoLang, integrated various Blockchain protocols into Ubiquity (a Blockchain aggregation framework)', date_ts('2019-04'), NULL);
END $$;

DROP FUNCTION date_ts;