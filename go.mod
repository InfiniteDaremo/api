module gitlab.com/InfiniteDaremo/api

go 1.15

require (
	github.com/deepmap/oapi-codegen v1.4.2
	github.com/friendsofgo/errors v0.9.2
	github.com/getkin/kin-openapi v0.26.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/lib/pq v1.9.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	github.com/volatiletech/null/v8 v8.1.0
	github.com/volatiletech/sqlboiler/v4 v4.4.0
	github.com/volatiletech/strmangle v0.0.1
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
)
